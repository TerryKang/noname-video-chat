package com.example.bcit.terry.noname;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private NoNameVOIP app;
    private Socket mSocket;
    private UserListAdapter userListAdapter;
    private ArrayList<UserListItem> userList;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public UserListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserListFragment newInstance(String param1, String param2) {
        UserListFragment fragment = new UserListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        app = (NoNameVOIP)getActivity().getApplication();
        mSocket = app.getSocket();
        mSocket.on("online", onlineUser);
        mSocket.on("offline", offlineUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_list, null);
        ListView listView = (ListView)view.findViewById(R.id.onlinUserList);
        userList = app.getUserList();
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
         //       android.R.layout.simple_list_item_1, listItems);
        userListAdapter = new UserListAdapter(getActivity(), R.layout.item_user_list, userList) ;

        listView.setAdapter(userListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item index
                int itemPosition     = position;
                // ListView Clicked item value
                String  itemValue    = (String)parent.getItemAtPosition(position);
            }
        });
        Log.d("Fragment onCreateView", "UserListFragment");

        return view;
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d("Fragment Destroy", "UserListFragment");
        mSocket.off("online", onlineUser);
        mSocket.off("offline", offlineUser);
    }

    private Emitter.Listener onlineUser = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String username = (String) args[0];
                    UserListItem newUserItem = app.addNewUserItem(username);
                    userList.add(newUserItem);
                    userListAdapter.notifyDataSetChanged();
                }
            });
        }
    };
    private Emitter.Listener offlineUser = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(args[0].toString(), "offlineUser");
                    String username = (String) args[0];
                    app.removeUserItem(username);
                    for(int i=0; i < userList.size(); i++){
                        if(userList.get(i).getText().equals(username)){
                            userList.remove(i);
                            break;
                        }
                    }
                    userListAdapter.notifyDataSetChanged();
                }
            });
        }
    };

}
