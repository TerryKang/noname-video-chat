package com.example.bcit.terry.noname;

import android.graphics.drawable.Drawable;

/**
 * Created by Terry on 2016-10-09.
 */
public class UserListItem {
    private Drawable iconDrawable ;
    private String textStr ;

    public void setIcon(Drawable icon) {
        iconDrawable = icon ;
    }
    public void setText(String text) {
        textStr = text ;
    }

    public Drawable getIcon() {
        return this.iconDrawable ;
    }
    public String getText() {
        return this.textStr ;
    }
}