package com.example.bcit.terry.noname;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class UserListAdapter extends ArrayAdapter  {
    public interface ListBtnClickListener {
        void onListBtnClick(int position) ;
    }

    int resourceId ;
    private Context m_context;



    UserListAdapter(Context context, int resource, ArrayList<UserListItem> list) {
        super(context, resource, list) ;
        m_context = context;
        this.resourceId = resource ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position ;
        final Context context = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(this.resourceId/*R.layout.listview_btn_item*/, parent, false);
        }

        final ImageView iconImageView = (ImageView) convertView.findViewById(R.id.imageView1);
        final TextView textTextView = (TextView) convertView.findViewById(R.id.textView1);

        final UserListItem listViewItem = (UserListItem) getItem(position);

        iconImageView.setImageDrawable(listViewItem.getIcon());
        textTextView.setText(listViewItem.getText());

        ImageButton button = (ImageButton) convertView.findViewById(R.id.call_button);
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {

                ((NoNameVOIP) m_context.getApplicationContext()).callTo(textTextView.getText().toString(), view);
            }
        });

        return convertView;
    }

}