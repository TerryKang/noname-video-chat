package com.example.bcit.terry.noname;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabPagerAdapter extends FragmentPagerAdapter {

    int tabCount;

    public TabPagerAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        if (fm.getFragments() != null) {
            fm.getFragments().clear();
        }
        this.tabCount = 2;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                UserListFragment tab1 = new UserListFragment();
                return tab1;
            case 1:
                ChatFragment tab2 = new ChatFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}

