package com.example.bcit.terry.noname;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;

import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Socket;

/**
 * Created by Terry on 2016-10-09.
 */
public class NoNameVOIP extends Application{
    private Socket mSocket;
    private ArrayList<String> onlineUsers;
    private ArrayList<UserListItem> userList;
    private String username;
    private int numUsers;
    {
        try {
            mSocket = IO.socket("http://54.200.160.119:4000");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
    private static Context context;
    private PopupCalling popupCalling;
    public void onCreate() {
        super.onCreate();
        NoNameVOIP.context = getApplicationContext();
        popupCalling     = new PopupCalling(context);
    }

    public void callTo(final String to, View view){
        mSocket.emit("sendMessage",to, "{ type: 'call' }");
        popupCalling.callTo(to, view);
    }
    public void callFrom(final String from, View view){
        popupCalling.callFrom(from, view);
    }
    public void answerCall(final String user){
        mSocket.emit("sendMessage",user, "{ type: 'answer' }");
        startCall(false);
    }
    public void ignoreCall(final String user){
        mSocket.emit("sendMessage",user, "{ type: 'ignore' }");
    }

    public void endCall(){
        popupCalling.endCall();
    }

    public void startCall(boolean isCalling){
        Intent intent = new Intent(context, CallActivity.class);
        intent.putExtra("myName", username);
        intent.putExtra("callerName", popupCalling.caller);
        Log.d("NoNameVOIP", "isCalling : " + isCalling );
        intent.putExtra("isCalling", isCalling);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        endCall();
        startActivity(intent);
    }

    public Socket getSocket() {
        return mSocket;
    }

    public void setOnlineUsers(JSONArray jsonArray){
        int arraySize = jsonArray.length();
        userList = new ArrayList<UserListItem>() ;
        onlineUsers = new ArrayList<String>() ;
        for(int i=0; i<arraySize; i++) {
            try{
                String user = (String) jsonArray.getString(i);
                Log.d("setOnlineUsers" , "user : " + user + ", username : " + username);
                if(user.equals(username))
                    continue;
                userList.add(addNewUserItem(user)) ;
            }catch(org.json.JSONException e){
                Log.d(e.getMessage(), "ERROR");
            }
        }
        numUsers = userList.size();
    }

    public UserListItem addNewUserItem(String username){
        onlineUsers.add(username);
        numUsers++;
        UserListItem item = new UserListItem() ;
        item.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_account_circle_black)) ;
        item.setText(username) ;
        return item;
    }

    public void removeUserItem(String username){
        onlineUsers.add(username);
        for(int i=0;i<userList.size();i++){
            if(userList.get(i).getText().equals(username)){
                userList.remove(i);
                onlineUsers.remove(i);
                numUsers--;
                break;
            }
        }
    }

    public ArrayList<String> getOnlineUsers(){
        return onlineUsers;
    }

    public ArrayList<UserListItem> getUserList(){
        return userList;
    }

    public void setUserName(String name){
        username = name;
    }
    public String getUserName(){
        return username;
    }
    public int getNumUsers(){return numUsers;}
}
