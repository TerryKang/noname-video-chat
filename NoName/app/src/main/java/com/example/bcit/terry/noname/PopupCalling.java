package com.example.bcit.terry.noname;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * Created by Terry on 2016-11-13.
 */
public class PopupCalling extends PopupWindow {
    Context m_context;
    ImageButton answerButton;
    ImageButton endButton;
    ImageButton ignoreButton;
    NoNameVOIP app;
    String caller;
    boolean isBeingCalled = false;
    boolean isCalling = false;

    public PopupCalling(Context context)
    {
        super(context);

        m_context = context;
        app =
        ((NoNameVOIP) m_context.getApplicationContext());
        setContentView(LayoutInflater.from(context).
                inflate(R.layout.popup_calling, null));

        setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        answerButton = (ImageButton)getContentView().findViewById(R.id.answerButton);
        endButton = (ImageButton)getContentView().findViewById(R.id.endButton);
        ignoreButton = (ImageButton)getContentView().findViewById(R.id.ignoreButton);


    }

    public void close(){
        dismiss();
    }
    public void callTo(final String user, View anchor)
    {
        isCalling = true;
        caller = user;
        answerButton.setVisibility(View.GONE);
        ignoreButton.setVisibility(View.GONE);
        endButton.setVisibility(View.VISIBLE);
        ((TextView)getContentView().findViewById(R.id.callingText)).setText("Calling to " + caller);
        dismiss();
        showAtLocation(anchor, Gravity.CENTER, 0, 0);
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.ignoreCall(caller);
                endCall();
            }
        });
    }
    public void callFrom(final String user, final View anchor)
    {
        if(isCalling || isBeingCalled)
            return;
        isBeingCalled = true;
        answerButton.setVisibility(View.VISIBLE);
        ignoreButton.setVisibility(View.VISIBLE);
        endButton.setVisibility(View.GONE);
        caller = user;
        ((TextView)getContentView().findViewById(R.id.callingText)).setText("Calling From " + caller);
        dismiss();
        showAtLocation(anchor, Gravity.CENTER, 0, 0);
        answerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.answerCall(caller);
            }
        });
        ignoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.ignoreCall(caller);
                endCall();
            }
        });
    }

    public void endCall(){
        isBeingCalled= false;
        isCalling = false;
        dismiss();
    }
}
